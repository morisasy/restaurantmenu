from app import db


class Restaurant(db.Model):
	"""
		Create a Restaurant Table
	"""
	
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(250), nullable=False)
	menuitems= db.relationship('MenuItem', backref='restaurant', lazy='dynamic')

	def __repr__(self):
		return '<Restaurant {}>'.format(self.name)


class MenuItem(db.Model):
	"""
		Create a Menu Items Table
	"""
	
	name = db.Column(db.String(80), nullable=False)
	id = db.Column(db.Integer, primary_key=True)
	description = db.Column(db.String(250))
	price = db.Column(db.String(8))
	course = db.Column(db.String(250))
	restaurant_id = db.Column(db.Integer, db.ForeignKey('restaurant.id'))
	#restaurant = relationship(Restaurant)

	def __repr__(self):
		return '<MenuItem {}>'.format(self.name)

"""
engine = create_engine('sqlite:///restaurantmenu.db')
class Employee(Base):
	
	name = Column(String(250), nullable=False)
	id = Column(Integer, primary_key= True)

	def __repr__(self):
		return '<Employee {}>'.format(self.body)

class Address(Base):
	
	street = Column(String(80), nullable = False)
	zip = Column(String(5), nullable = False)
	id = Column(Integer, primary_key = True)
	employee_id = Column(Integer, ForeignKey('employee_id'))
	employee= relationship(Employee)

	def __repr__(self):
		return '<Address {}>'.format(self.body)
"""
	#engine = create_engine('sqlite:///employeeData.db')
	#Base.metadata.create_all(engine)